---
title: What to negotiate in a franchise agreement as a franchisee.
date: 2019-01-20 15:13:29 UTC
layout: article
---

In franchise arrangement, a power imbalance is unavoidable between a Franchisor
and a Franchisee. The Franchisor has more power to push certain standards and
requirements for its potential franchisee(s), because the Franchisor would like
to have quality control over its products, services, and goodwill that have been
established.

<!-- READ MORE -->

Some jurisdictions require pre-contractual disclosure. In this stage the
Franchisor position is fragile, because the Franchisor may have to disclose
certain information about its organization and/or the business such as: business
experience, any litigation cases, payment information, expected earning
information, or any other financial details. As a result, confidentiality is the
core element for the protection of the Franchisor's interest, though that does
not preclude the Franchisee to work on certain terms.

The following points are crucial when negotiating the terms and conditions in a
franchise agreement as a Franchisee.

# Financial Terms

Aside from the Franchisee Fee, it's common to find another recurring fee, such
as a Royalty Fee or Advertising Contribution. The Royalty Fee typically comes
with a certain percentage of Gross Sales or Net Sales. The terms of gross or net
sales do not matter provided you negotiate the definition of them. It's useful
to exclude the employee’s discount, taxes, complimentary products, tips, and any
other discount for third parties in the definition of such sales (whatever term
you decide to use in the agreement), as the basis of calculating the Royalty
Fee.

Advertising Contributions may also take up a certain percentage of the above
sales. Typically the Franchisor collects the amount paid by all the franchisees,
and creates an advertising program either in local, national, or international
scope. There might be the possibility to opt-out from this obligation with a
firm justification. For example, the advertising program may not affect the
sales of the franchised business for a specific location.

Anything that is not defined is open to interpretation. In the event the
Franchisor requires the Franchisee to bear any cost incurred by the Franchisor
due to various reasons, you may want to limit the scope of the costs. Such costs
may exclude fees for consultants, advisors, or lawyers of the other party.

# Liability

It makes sense that Franchisor wishes the Franchise to indemnify them for all
losses or damages of the franchised business, unless due to _force majeure_.
Some jurisdictions may regulate the scope of damages. Having a good
understanding on the governing law of the contract would be the ideal situation.
You can always contact a lawyer. In any case, it's advisable to limit the
liability scope. What the Franchishor might approve is a proposal of a mutual
waiver provision. For limiting the scope of damages, you may use the sample
below:

> Both parties, to the fullest extent permitted by applicable law, waive any
> right for exemplary, indirect, treble, punitive, or any other kind of multiple
> damages against the other party and agree that in the event of a dispute
> between the parties, the Party making a claim will be limited to equitable
> relief and to recovery of any actual damages it sustains.

# Operational Terms

The main characteristic of the Franchisor is that they have established a
succesful business concept. By entering into a franchise arrangement, the
Franchisor grants a right to the franchisee to use the business system,
including the use of their intellectual property, such as trademark or trade
name. Within its system, the Franchisor will have extensive and detailed
information on how to operate their business. It typically includes (but is not
limited to) the supply chain, standard operation procedures, system, policies,
methods, and any other information.

## Supplier

The Franchisor may require the Franchisee to purchase certain products,
ingredients, or equipment from its designated suppliers. When the Franchisee is
a well-established franchise operator, it's common that it has established
a relationship with some suppliers. The use of the franchisee's own suppliers
could in this case be beneficial.

## Provision on the premises

When the occupancy rights of the franchised business location come from a lease
or concesssion arrangement with a building manager, the building manager may set
specific provision about the premises, for example, opening hours.
Meanwhile, the Franchisor may require different opening hours. These provisions
should not conflict, otherwise it may be in breach of one of the contracts.

## Refurbishment

In food and beverages or retail business, distinguished interior and exterior
design of the premises also play a role in attracting the customers. The
Franchisor has adequate control over this. The Franchisor could require some
refreshment to the design that may have worn out.

In a long term arrangement, the Franchisor may ask for a major refurbishment and
the Franchisee needs to negotiate the terms as it may cost some capital
expenditure. For example, if the contract is for 10 years, then the
refurbishment could be every 5 years of operation.

# Restrictive Covenant

The most common provision is the non-compete prohibiting the Franchisee from
engaging a competing business, often such a business that may be confusingly
similar to the Franchisor's business. It could be for a certain period of years,
a specific distance restriction, or certain products from a competing business.
It could be a hassle if the Franchisee is not a sole purpose franchisee. As
advised, you may propose a scope of competing business which exclude certain
products or services that the Franchisee sells with different franchisor, as the
case may be.

# Cross-Checking

Franchisee does not always have a master franchise agreement. Further, it's also
practical to have a franchise agreement for each location. In this case, it's
better to cross check with those franchise agreements, or even the agreement in
relation to the location.

In relevance to the other franchise agreement, sometimes there is a
cross-default provision where the Franchisor has right to terminate the
agreement if the Franchisee is in default. On the other side, what I typically
find in the retail franchise business is that the building manager has set
certain rules on the technical matters, for example Point of Sale (POS) System.
Making sure that they don't conflict one another is advisable.

# Disclaimer

In the stage of the negotiation, the parties may overcome different situations
depending on various factors. There might be more things to take into account
than what we have discussed so far. The purpose of this article is to provide
general information. This article does not constitute a legal advice or opinion
whatsoever. I recommend seeking specific legal advice.