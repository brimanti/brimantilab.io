---
title: 'Fiduciary Security Rights (Indonesian Law vs Dutch Law)'
date: 2020-04-02 23:09:00 UTC
layout: article
---

Since 1992, Dutch law has taken different direction when it comes to fiduciary
security concept. It is called "fiducia-ban". Meanwhile Indonesia has strengthen
the existence of fiduciary security through Law No. 42 of 1999.
This article will discuss about the concept of fiduciary security rights in both
countries.

<!-- READ MORE -->

# Short history of fiduciary security existence

It is understood that Indonesia legal system has a close relationship with the
Dutch law. In fact the main source of Indonesian civil law ("ICC"), is translated
from Burgerlijk Wetboek, an old Dutch Civil Code. However, since 1992 Dutch
legal system developed to adopt its new regime under the New Dutch Civil Code
(“NCC”). At this point, Indonesia and the Netherlands have been going to a
different path for several matters because Indonesian law remains using the ICC
with some adjustments. Specifically on security proprietary rights, both in
Indonesia and the Netherlands used case laws as the basis of the fiduciary
security existence. Key case:

In the Netherlands :
-	Bierbrowrij Arrest dated 25 January 1929
-	Hakkers-van Tilburg Arrest dated 21 June 1929

In Indonesia :
- the ruling of Hooggerechtschof (Hgh) in the case of Bataafsche Petroleum
  Maatschappij (“BPM”) v Pedro Clignett, dated 18 August 1932

The judges’ findings have developed in Indonesia. Several rulings enacted to
strengthen the legal basis of the existence of fiduciary. However, as a civil
law country, the precedent will never bind the court. In 1999, Indonesian
legislator enacted Law No. 42 of 1999 on Fiduciary Security (“Indonesian
Fiduciary Law”) to reaffirm its existence and provide a distinctive legal basis
to protect the parties in a loan agreement. This legal instrument was born right
after major crisis in Indonesia in late 1997-1998. On May 1998, the regime of
the incumbent president, Soeharto was tackled down. Riots were rampant in many
parts of Indonesia and racial and religious tensions rose. At this moment,
Indonesia became a worrying place for investors and fly-in travelers. The
issuance of the legal basis for fiduciary security has been accommodating the
entrepreneurs who suffered from the crisis. In the contrary to the development
in Indonesia, the concept of fiduciary security is prohibited under the Dutch
Law since 1992.  See the section below (Fiducia-ban:New regime of Fiduciary
Security under Dutch Law (NCC).

# Characteristic of Fiduciary Security under Indonesian Fiduciary Law

Definition: The term of ‘fiduciary’ is defined in Art. 1 Indonesian Fiduciary
Law as:“transfer of ownership of an object on trust with the provision that
transferred ownership of the object remains in the control of the owner of the
object.”It is indeed deliberating that the legislator finally promulgated
fiduciary security as one of solution for the entrepreneurs to survive their
business in economic crisis. Through fiduciary security, for instance, a
businessman could be able to place its inventories as security for a loan to the
lender without having its inventories to be transferred physically to the
lender. The businessman could remain to use its inventories to run his business
to pay his debt.

The parties: Fiduciary Law categorizes the parties in a fiduciary arrangement
not only as creditor and debtor (“Debtor”) but also fiduciary transferor and
fiducia transferee. However, the fiducia transferee has the same position as a
creditor (“Creditor”). Meanwhile, the fiduciary transferor shall be a natural
person or corporation owner of fiduciary objects (“Transferor”). In other words,
Fiduciary Law provides the possibility that third party could be involved as
third party Transferor in order to secure the Debtor’s indebtedness provided
that the Transfer is the owner of the fiduciary object.

The objects: Fiduciary objects cover any goods that can be owned and transfer
either tangible or intangible, registered or unregistered that cannot be
encumbered by mortgage and hypothec. Although, immovable goods such as ownership
right over apartment built above right of use over State of Land or ownership
right over machinery attached to factories can also be the object of fiduciary
security.

The nature: Under Article 4 of Fiduciary Law, a general characteristic of
fiduciary security is an accessory to a loan agreement as underlying act.
Further, it shall follow the fiduciary object regardless which party possessed
the object under principle of “droit de suiteI” or “zaaksgevolg” [1]. A. Pitlo,
a Dutch scholar explained “droit de suite” as the right of a creditor to pursue
debtors property into the hands of third persons for the enforcement of his
claim. This principle used as protection for the Creditor under fiduciary

Another main characteristic of fiduciary security is to give a rise of
preference rights to the Creditor who holds the registered fiduciary security.

# Encumbrance of Fiduciary Security.

Establishment of fiduciary security comes after the underlying agreement, for
instance loan agreement had entered by debtor and creditor. Type of indebtedness
can be attached by fiduciary arrangement is not only the existing debt, but also
upcoming debt which has been agreed on certain date and amount and debt which
its amount including the interest can be determined based on the underlying
agreement which gives rise to a person to fulfill his obligation. A fiduciary
security agreement may include one or more type of goods. Having said that, it
is possible that a single notarial deed regarding fiduciary security mentioned
more than one type of goods provided that all the fiduciary object shall be
described and along with the ownership certificate. In the case of the fiduciary
object is inventories, capital goods, securities company portfolio which in
nature fluctuate and non-fixed, the deed of fiduciary security shall at least
contain the information on the type, brand, quality over such goods.

# Registration of Fiduciary Security

Fiduciary security shall be effective as the date of registration in Fiduciary
Book. Encumbering the fiduciary security shall be perfected by registration.
Failure to comply with this condition will eliminate rights of the Creditor to
enforce the security if the Debtor is in default. Upon the issuance of
Government Regulation No. 21 of 2015 on Procedures for Fiduciary Security
Registration and Deed of Fiduciary Security Establishment Fee, starting from 6
April 2015 fiduciary registration can be applied online by appointed Notary who
made the deed of fiduciary security through a website
https://fidusia.ahu.go.id/.

# Prohibition under Fiduciary Law

It is strictly prohibited for Transferor to have double encumbrance over goods
that has been registered as fiduciary security. Otherwise, the new Creditor will
lose his right to enforce the security in case of default. See case ABNR vs
Sumatra Partners LLC. Fiduciary Law also prohibits the Transferor to assign,
pledge, or lease the fiduciary object (other than inventories) to another party.
Failure to comply with this provision, the Transferor may be imposed with
imprisonment for maximum 2 of (two) years and fine for a maximum of
IDR50,000,000 or equivalent approximately EUR3,500. The Transferor can only
assign the inventories if the Debtor or Transferor is not in default. Assignment
of the inventories arises another obligation of the debtor or Transferor to
replace the inventories with the same value object. In the event of default, the
result of the assignment (e.g., money from the sale and purchase the object) or
invoice arise from such assignment will be treat as the replacement of the
assigned fiduciary object.

# Execution and abolishment of Fiduciary Security Rights

In the case of the Debtor’s default, the Creditor has three options to execute
the security.

1.  Implementation of executorial title. However, after the issuance of
    Constitutional Court Decision No.18/PUU-XVII/2019 issued on 6 January 2020
    that executorial title can only be executed once the there is a court
    decision stating that the Debtor is in default. In short, Creditor cannot
    unilaterally determine that Debtor’s in in default.
2.	Sale security object through public auction.
3.  Sale security object in private sale based on Transferor and Creditor
    consensus provided that such sale could reach highest selling price for the
    benefit of both parties.

The legislator limits the condition for abolishing the fiduciary security to
three circumstances:

1.	the Debtor has fully paid his indebtedness to the Creditor.
2.	if the Creditor releases his fiduciary rights.
3.	delimitation of the fiduciary object.

Note that given the accessory nature to its underlying agreement, when the
underlying agreement is null the fiduciary security shall be automatically
abolished. In the case of the last circumstance, the parties should understand
that if only part of the fiduciary object is destroyed the remaining fiduciary
object should stay its validity. As a result, both fiduciary security agreement
and its underlying remain valid [2]. In any case the demolished fiduciary object
is insured, the insurance claim will be treat as substitution of such object.

# Fiducia-ban: New regime of Fiduciary Security under Dutch Law (NCC)

Since the enactment of NCC in 1992, the Dutch legislators prohibit the fiduciary
transfer of ownership through Article 3:84(3) NCC so called “fiducia-ban”
stating that:“A juridical act performed with intention to transfer property
merely to provide security for a debt or performed without the intention of
bringing the property into the patrimony (capital) of the acquirer, is no valid
legal basis for a transfer of that property.”

The idea of fiducia-ban resulted from debates between the Dutch scholars on the
concept of ownership rights. Under the NCC, ownership is the most comprehensive
right that a person can have in a thing. Ownership is also described as absolute
in the Roman-Dutch tradition to indicate that it permits the owner to do with
the property what he likes or that ownership is presumed to be free of
restriction as a point of departure, with the result that restrictions can be
imposed (by law or by consent) but will be treated as temporary and exceptional
[3]. Civil law also adopts the unitary concept of ownership. The idea that all
possible ownership entitlements are usually vested in one person – that there is
just one owner for a particular item of property and the view that the owner is
entitled to exclusive (disposition over) possession and use of his property are
traditionally associated with the civil law definition of ownership as an
absolute right, resulting broad assumption that the owner is generally entitled
to exclude anyone else from access to and possession of his property [4]. On the
other hand, the structure of fiduciary transfer seems to limit the ownership
rights. Knowing that the Creditor as the legal owner, he supposed to able to
transfer the goods to third parties. However, the fiduciary agreement limits the
Creditor’s rights of disposal. In this case, fiduciary transfer of ownership
obscure the absoluteness of ownership rights with the fact that the Creditor as
the legal owner but his rights over the goods is restricted.

It is therefore the concept of fiduciary transfer of ownership is in contrary
with the legal concept of Civil Law. As a result, fiducia-ban exists. For this
purpose, see the substitution to fiduciary concept below.

Substitution to Fiduciary concept: The legislator compensated the prohibition of
fiduciary security with the introduction of a general right of “silent”
non-possessory pledge as regulated in Article 3:237 and 3:239 of NCC.36
Generally, NCC distinguishes two types of pledge, such as:
-	possessory pledge (vuistpand); and
-	non-possessory pledge (bezitloos pandrecht).

Particularly in non-possessory pledge, the establishment of such pledge is
created by either a notarial deed before a notary or privately executed
instrument that is subsequently registered with tax authorities. This depends on
the nature of pledge, if it is disclosed to the debtor, a right of the pledge
shall be established by a private deed and notice thereof to the relevant
debtor, or if undisclosed to the debtor, a right of pledge must be established
by an authentic deed or registered private deed without notification thereof to
the relevant debtor [5]. Undisclosed pledges can only be created over existing
receivables or over future receivables that directly derive from a legal
relationship existing at the time of the execution of the pledge [6]. No public
registration requirement is usually referred to as “silent” non-possessory
pledge.  The registration with tax authorities will only serve for stamping and
establishing the exact date of security establishment. Given this, the Creditor
will be entitled to personal rights due to the non-possessory pledge and the
ownership right over the object shall remain in the Debtor or third party
Transferor.

Execution of the pledge: In case of the Transferor is in fails to perform his
obligation for which the pledge serves as security, the Creditor is entitled to
sell the pledged property in public. However, the court may determine that the
pledged property will be sold in another manner, for instance, a private sale
upon the Transferor or Creditor’s request. Moreover, upon the Creditor’s request
the court may also determine that the pledged property will remain with the
Creditor as buyer for an amount to be determined by the court) [7].

# Closing & Disclaimer

It is interesting to see how both countries have taken difference approach from
one another for its legal development. On one hand Indonesia strengthen the
existence of fiduciary security arrangement by issuing the Fiduciary Law in
1999, on the other hand Dutch Law has banned the fiduciary concept. The
fiduciary security in Indonesia helps many entrepreneurs to keep running their
business. Although along the way, in practice the fiduciary security has caused
some dispute but the development of registration system hopefully could mitigate
its potential issue. The purpose of this article is to provide general
information. This article does not constitute a legal advice or opinion
whatsoever. I recommend seeking specific legal advice.

[1] Kamelo, Tan. Hukum Jaminan Fidusia: Prinsip Publisitas pada Jaminan Fidusia,
at 161 (2015)

[2] J. Satrio, Hukum Jaminan, Hak-Hak Jaminan Kebendaan. at 304 (1991) 

[3] Van der Walkt, Andries Johannes. Property in the Margins, at 34 (2009)

[4] Walkt, Van Der, Supra note 3, at 34

[5] Buruma, Houthoff, Security over Collateral, available at
http://webcache.googleusercontent.com/search?q=cache:d7xHhDRnNJMJ:www.lexmun
di.com/Document.asp%3FDocID%3D1029+&cd=1&hl=id&ct=clnk&gl=ch

[6] van Rossum, Stefan & Botter, Van Doorne NV, Country Q & A the Netherlands,
2008 available at
https://webcache.googleusercontent.com/search?q=cache:VOFw9xROHAgJ:https://www.van
doorne.com/contentassets/888eb2e00da942bfaec9420ca16c9402/vanrossum-
2.pdf+&cd=1&hl=id&ct=clnk&gl=ch 

[7] Wolfgang, Faber & B. Lurger, Rules for Transfer of Movables: a Candidate for
European Harmonisation or National Reform, at 171(2007) 
















