---
title: Warning letters for a misconduct under Indonesian Manpower Law
date: 2020-04-26 22:07:29 UTC
layout: article
---

When an employee commits a misconduct the employer must issue the warning
letter(s) before initiating a dismissal of employment. The warning letter(s) can
be issued in stages. The question is how does it work? This article will answer
it.

<!-- READ MORE -->

Indonesian law gives some level of protection for employees from termination
employment. Unlike in the United States where it is fairly easy to fire
employee(s). The general principle for termination in Indonesia is that the
employer, the employeer and/or the labour union, and the government must make
all efforts to prevent termination of employment from taking place.
Specifically, an employment contract can only be terminated for certain reasons
and immediate termination is strictly prohibited. To dismiss an employee, an
employer has to follow certain procedures subject to the ground of dismissal. In
case of an employee violates the company's code of conduct or terms of
employment, employer has to give the relevant employee with warning letters
before initiating the termination.

Typically, an employer has a code of conduct that elaborate the "Do" and
"Don'ts" that are applicable not only for employees but also for employer
itself. Such code of conduct may be speficied in the employment agreement,
Company Regulations <i>(Peraturan Perusahaan)</i>, and/or Collective Labour
Agreement <i>(Perjanjian Kerja Bersama)</i>.

  <b>Note</b>: When a company has a labour union, it is important to check
  whether Collective Labour Agreement is in place. It may contain further
  provisions on this. Otherwise, note that every employer that hires more than
  10 employees must have a Company Regulation which needs to be legalised by the
  relevant manpower institution. Please check the relevant internal regulations
  in your company whether be Company Regulations or Collective Labor Agreement.

Most of the time an employment agreement does not give greater details on the
code of conducts. We often see those are regulated either in Company Regulations
or Collective Labour Agreement.

# Warning Letters (1st, 2nd, 3rd)
The general rule is contained in Article 161 paragraph (1) of Indonesian
Manpower Law. In summary it says that if an employee violates any of the code of
conduct, the employer may terminate his/her employment after giving the employee
with first, second and third (final) warning letters which ideally shall be
given consecutively. Each warning letter must be effective for maximum of 6
months, unless regulated otherwise.

The main idea is that if after the third warning letter is given, the employee
keeps violating the code of conduct within the period of the third warning
letter, only then the employer can terminate the employment relationship.

The above statements may arise few questions either by employer or employees.
Please see Q&A below for clarification.

## Can the effective period of a warning letter be less than 6 months?
Yes, Indonesian Manpower Law only limits the maximum period (eg 6 months). If
the employer wants to set the period differently it has to be written in
either employment agreement, Company Regulations or Collective Labour
Agreement.

Note that the period of warning letter cannot be more than 6 months. This
serves as a protection. Please see the subsection below for more elaboration.

## How to issue the consecutive warning letter(s) if the employee keep committing misconduct?
The elucidation of Article 161 paragraph (2) of Indonesian Manpower Law gives
an example as below:

Suppose an employee commits a misconduct, then the first warning letter shall
be effective for a period of 6 (six) months. If the employee commits a
misconduct again within the period of 6 months of the first warning letter,
the employer may issue the second warning letter. The second warning letter
shall be also effective for 6 months. If the employee keeps committing a
misconduct within the period of second warning letter, the third warning
letter may be issued. If the employees doest not improve within the period of
third warning letter, the employer may terminate the employment relationship.

As the rule of thumb, the warning letter can only be upgraded if the employee
keeps committing a misconduct within the effective period of the current
warning letter. If another misconduct happens outside the effective period,
the employer must start again from first warning letter.

Given the above, you can see that the law gives employee more opportunities to
improve his/her performance so that the termination of employment can be
avoided.

## Can employer immediately issue a third warning letter to employee?
Yes. It depends on the weight of misconduct, provided that it is stated in either
employment agreement, Company Regulations or Collaborative Labour Agreement.

For example, in one of those employment documents states that if an employee
uses the company's belonging or property for private interest, the employer
may issue a third warning letter. Therefore it is important to clearly break
down all type of misconducts that are applicable in either employment
agreement, Company Regulations or Collaborative Labour Agreement.

# Termination due to misconduct
The termination with the ground of misconduct can only be initiated if the third warning letter
has been issued. This comes with two exception.

First when both employee and employer can mutually agree with the termination of
employment in a bipartite meeting and conclude this into a written mutual
agreement. Such mutual agreement needs to be registered at the relevant
Industrial Court. I would not go deep into this as this will open more
discussion point. I will elaborate this in my upcoming article. Second, the
misconduct falls under scope of "gross misconduct" as specified in Article 158
(1) of Indonesian Manpower Law. Please see sub-section of <b>Gross
Misconduct</b>

If it is not considered gross misconduct, the employee will be entitled to one
time severance package <i>(Uang Pesangon)</i>, one time service period
recognition payment < i>(Uang Penghargaan Masa Kerja)</i>, and Compensation
<i>(Uang Penggantian Hak)</i> due to termination with ground of minor mistake as
stated in Article 161 paragraph (3) of Indonesian Manpower law.

## Gross Misconduct / Serious Misconduct
Under Indonesian Manpower Law, the scope of gross misconduct are as follows:

  1. swindling, theft and embezzlement of goods/cash owned by the Employer;
  2. providing fake or falsified information, which inflicts losses on the company;
  3. being drunk, drinking liquor, using or distributing narcotic, psychotropic and other addictive substances in the workplace;
  4. committing an indecent act or gambling at the work place;
  5. assaulting, intimidating, maltreating, or deceiving the Employer or his fellow Employees within the Company;
  6. persuading the Employer or fellow Employees to be engaged in an act against the law and the prevailing laws;
  7. recklessly or deliberately damaging goods belonging to the Employer, harming them or leaving them in dangerous and thereby causing a loss to the company;
  8. recklessly or deliberately leaving the Employer or fellow Employees in danger in the work place;
  9. divulging Company secrets, which should otherwise be kept confidential, except in the State’s interest; or
  10. committing other crimes in the Company premises liable to a prison sentence of 5 years or more.

As consequence, the employer can terminate the employment relationship for the
above grounds if it is supported with the following evidence:
1. the employee is caught red-handed;
2. the employee admits that he/she has committed a wrongdoing;
3. other evidence in form of reports of events made by the
authorised person at the company and confirmed by no less than 2 (two)
witnesses.

The employee who is dismissed due to this serious misconduct will be entitled
to a compensation <i>(Uang Penggantian Hak)</i> as stated in Article 158
paragraph (3) of Indonesian Manpower Law. If the employee's duties and
function do not directly represent the company's interest, she/he is also
entitled to Separation Pay <i>(Uang Pisah)</i> as stated in Company
Regulations or Collective Labour Agreement, if any.

# Permanent Employee vs Fixed-Term Employee
The rules about warning letters should be applicable for all type of employees.
We need to check the relevant provisions in the Company Regulations or
Collective Labour Agreement.

Permanent employees have more termination benefits compared to fixed term
employees. For termination of fixed term employee without any grounds, the
company has to pay the salary of the employees for the remaining term of the
contract. If the grounds of termination is a misconduct, we need to check
whether Company Regulations or Collective Labour Agreement gives the same
benefit as Permanent Employees as discussed above.

# Covid-19 situation (potential case)
In the midst of quarantine period, most of the employers have been requiring its
employees to work from home. In particular situation, it can be difficult to
determine whether an employee commits a misconduct, especially a minor mistake.

As an example, being absence without any written statement explaining why he/she
is absence for at least 5 (five) working days or more is one of valid grounds of
termination. With a working from home regime, it may be difficult to accuse an
employee has committed such misconduct. Ideally, the employer must have
established relevant code of conduct or guideline for this working from home
regime. For example, as a prove of attendance, the employee has to sign-in to
relevant platfrom (eg microsoft team, skype etc).

In worst case scenario when the employee is completely unreachable, the employer
can assume that the employee has resigned from the company provided the
following requirements are fulfilled;

1. the employer has summoned the relevant employee with a written letter minimum
   2 (two);
2. such summon letters are sent to the employee’s address; and
3. the period between each summon letter shall be at least 3 days.

As consequence, the employee is entitled to Separation Pay <i>(Uang Pisah)</i>
and Compensation <i>(Uang Penggantian Hak)</i> as specified in Article 168
paragraph (3) of Indonesian Manpower Law.

# Disclaimer
To get more clarity, it is important to check your employment contract, Company
Regulations <i>(Peraturan Perusahaan)</i> or Collaborative Labour Agreement
<i>(Perjanjian Kerja Bersama)</i>, as appropriate. When in doubt, it is also
advisable to contact your Human Resources staff in your office. The purpose of
this article is to provide general information. This article does not constitute
a legal advice or opinion whatsoever. I recommend seeking specific legal advice.