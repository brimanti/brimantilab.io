---
title: Brimanti Kartamedja
created_at: 2019-01-13 15:04:18 UTC
description: Brimanti's personal website. Add more details here.
---

I am a lawyer and legal consultant with almost 10 years of experience. I have a
broad general legal experience with a focus on contract law. I have approx. 7 years of experience working for muItinational companies in field of hospitality and biotechnology manufacturing company, including pharmaceutical, biomedical, food ingredients products.
I hold an Indonesian advocate license. To see the kind of clients and
work that I have been involved with, please visit my
[LinkedIn](https://www.linkedin.com/in/brimantidebruin) page.

I speak English, Indonesian, Malaysian, and Dutch (B2 Level). I love reading
books about history, religions, mythology and other non-fiction. Writing is also one
of my passion and in fact I am a contributor of
[Soul Travellers](https://www.goodreads.com/book/show/36126648-soul-travellers)
book that is published in 2017.

I create this personal website mainly to publish my articles in legal related
matter. Please be aware that any of article in this website does not constitute
a legal advice or opinion whatsoever.

If you have any question, please contact me via
[Email](mailto:brimantisari@gmail.com).


# Education

* **LL.M (Master of Laws)** - Universiteit Leiden, The Netherlands.
  majoring in International Civil and Commercial Law.
* **LL.B (Bachelor Degree)** - University of Indonesia, Indonesia.
  majoring in Business and Civil Law.
