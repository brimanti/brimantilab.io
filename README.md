# README

This repository contains the source code of my personal website.

# Writing

1. Open a terminal in Visual Studio Code: Click "Terminal" in the grey Mac bar
   at the top of the screen, then choose "New Terminal".

2. In the terminal, type `rake server` and press Enter. This will start a
   program that will update a local copy of the website every time you make a
   change.

3. If you want to create a new article, type `rake article[Title here]` and
   press Enter. "Title here" should be the title of the article, such as "Why
   cats are awesome". You can then find the article in
   `source/articles/why-cats-are-awesome.html.md`.

   You only need to do this when creating the article for the first time. If you
   want to edit it afterwards, you can just edit the file directly.
   
4. If you typed `rake server` before and you want to run `rake article` instead,
   click on the terminal and press Control + C first.

# Requirements

* Ruby 2.5 or newer
* Bundler

# Development

Install Bundler:

    gem install bundler

Install all dependencies:

    bundle install

# License

This Source Code Form is subject to the terms of the Mozilla Public License, v.
2.0. If a copy of the MPL was not distributed with this file, You can obtain one
at http://mozilla.org/MPL/2.0/.
