# frozen_string_literal: true

require 'uglifier'

Haml::TempleEngine.disable_option_validator!

page '/*.xml', layout: false
page '/*.txt', layout: false
page '/*.ico', layout: false
page '/404.html', layout: :'404', directory_index: false
page '/', layout: :home

Time.zone = 'UTC'

set :website_title, 'Brimanti'
set :website_author, 'Brimanti'
set :website_url, 'https://brimanti.com'
set :feed_url, "#{config[:website_url]}/feed.xml"
set :markdown_engine, :kramdown

set :markdown,
    fenced_code_blocks: true,
    parse_block_html: true,
    auto_ids: true,
    auto_id_prefix: 'header-',
    tables: true,
    input: 'GFM',
    hard_wrap: false,
    toc_levels: 1..3

set :haml, format: :html5

activate :blog do |blog|
  blog.prefix = 'articles'
  blog.sources = '{title}.html'
  blog.permalink = '{title}/index.html'
  blog.layout = 'articles'
  blog.summary_separator = '<!-- READ MORE -->'
end

activate :directory_indexes

configure :development do
  activate :livereload
end

configure :build do
  set :build_dir, 'public'

  activate :minify_css
  activate :minify_javascript, compressor: proc { Uglifier.new(harmony: true) }
  activate :asset_hash
end

helpers do
  def markdown(text)
    Tilt['markdown'].new(config.markdown) { text }.render(self)
  end

  def navigation_options(path)
    current_page.path.start_with?(path) ? { class: 'active' } : {}
  end
end
